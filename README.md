# Prova de Vida

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.1.

## Servidor de desenvolvimento

Executar `npm start` para rodar o servidor em `http://localhost:4200/`.

### Rodar Keycloak local

Para rodar o Keycloak localmente, clone o projeto [Keycloak-temas](https://gitlab.com/supremotribunalfederal/corporativo/keycloak-temas) na branch _homologacao_ e execute `docker-compose up` na raiz do projeto.

Uma vez que o Keycloak esteja rodando no Docker, acesse `http://localhost:8080/admin` com o usuário padrão (usuário **admin** senha **admin**) e selecione *Clients* na barra lateral esquerda. Selecione então a opção *Import client* e adicione o arquivo `frontend.json`, disponível na raiz deste projeto. Mantenha as configurações padrões e salve ao final da página. Isso irá configurar o client *frontend* no realm Master.

Para apontar as rotas do Angular para exigirem autenticação pelo Keycloak local, adicione as seguintes configurações ao arquivo `assets/config/config.dev.json`:

```
{
    "KEYCLOAK_URL": "http://localhost:8080",
    "KEYCLOAK_REALM": "master",
    "KEYCLOAK_CLIENT_ID": "frontend"
}
```

Com essas alterações é possível utilizar esta aplicação e o Keycloak na máquina local. Lembrando que para acessar é necessário usar um usuário padrão, pois o Keycloak não irá acessar a base de usuários do STF.

## Testes unitários

Executar `ng test` para rodar testes unitários via [Karma](https://karma-runner.github.io).

## Observações

As configurações para autenticação através do Keycloak se encontram em `assets/config/config.*.json`. Ao entrar no componente prova-vida o servidor busca o arquivo de configurações do ambiente atual e prepara para checar a autenticação. 

Como a configuração de produção está como default, é necessário rodar o servidor a partir do comando `npm start`.