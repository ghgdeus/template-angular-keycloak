export interface Provadevida {
  matricula: string;
  sequencial: string;
  nome: string;
  numCpf: string;
  dataNascimento: string;
  situacao: string;
}
