import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvaVidaComponent } from './prova-vida.component';

describe('ProvaVidaComponent', () => {
  let component: ProvaVidaComponent;
  let fixture: ComponentFixture<ProvaVidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProvaVidaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProvaVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
