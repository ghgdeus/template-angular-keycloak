import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { ProvaVidaService } from './prova-vida.service';
import { Provadevida } from './model/provadevida';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-prova-vida',
  templateUrl: './prova-vida.component.html',
  styleUrls: ['./prova-vida.component.css']
})
export class ProvaVidaComponent implements OnInit {

  provadevida: Observable <Provadevida[]>;
  displayedColumns = ['matricula', 'sequencial', 'nome', 'numCpf', 'dataNascimento', 'situacao'];

  isNavbarCollapsed=true;

  loggedUserName = '';

  constructor(private keycloakService: KeycloakService, private service: ProvaVidaService) {
    this.provadevida = this.service.list();
  }

  ngOnInit(): void {
    this.loggedUserName = this.keycloakService.getUsername();
  }

  logout(): void {
    this.keycloakService.logout(window.location.origin);
  }

}
