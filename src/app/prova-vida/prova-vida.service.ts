import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { first, lastValueFrom, Observable, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { Provadevida } from './model/provadevida';

// TODO Alterar classe de teste
export interface Funcionario {
    matricula: string;
    nome: string;
}

@Injectable({
    providedIn: 'root'
})
export class ProvaVidaService {

    private readonly meuGovApi = environment.meuGovApi;

    private readonly endpoint = '/pessoa';

    private readonly API = '/pessoa';

    constructor(private http: HttpClient) {
     }

    getFuncionarios(): Observable<Array<Funcionario>> {
        return this.http.get<Array<Funcionario>>(this.meuGovApi + this.endpoint);
    }

    getFuncionarioByMatricula(matricula: string): Observable<Funcionario> {
        return this.http.get<Funcionario>(this.meuGovApi + this.endpoint );
    }

    list() {
      return this.http.get<Provadevida[]>(this.API)
      .pipe(
        first(),
        tap(provavida => console.log(provavida))
      );
    }
}
