export const environment = {
    production: true,
    configFile: 'assets/config/config.json',
    meuGovApi: '${MEU_GOV_API_URL}'
  };